# vuejs-starterkit

Forked from [VUE.js Starterkit](https://github.com/macedigital/vuejs-starterkit)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev
```
